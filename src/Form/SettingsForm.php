<?php

namespace Drupal\content_title_links_to_edition\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\views\Entity\View;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class of SettingsForm.
 *
 * @package Drupal\content_title_links_to_edition\Form
 */
class SettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Contructs SettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_title_links_to_edition.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_title_links_to_edition_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_title_links_to_edition.settings');

    $element_id = 'allowed_views';
    $element_wrapper_id = $element_id . '__wrapper';
    $rows = $this->getFormTableRows($element_id, $form_state);
    $form[$element_wrapper_id] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Views'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form[$element_wrapper_id][$element_id] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Enabled'),
        $this->t('View ID'),
        $this->t('View Title Column'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No entries available.'),
      '#attributes' => [
        'id' => 'table--allowed-views',
      ],
    ];
    for ($i = 0; $i < $rows; $i++) {
      $form[$element_wrapper_id][$element_id][$i] = [
        'enabled' => [
          '#type' => 'checkbox',
          '#default_value' => $config->get($element_id)[$i]['enabled'],
        ],
        'view' => [
          '#type' => 'textfield',
          '#default_value' => $config->get($element_id)[$i]['view'],
        ],
        'title' => [
          '#type' => 'textfield',
          '#default_value' => $config->get($element_id)[$i]['title'],
        ],
        'operations' => [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#name' => 'remove_' . $i,
          '#submit' => ['::removeRowAjax'],
          '#ajax' => [
            'callback' => '::allowedViewsAjaxCallback',
            'wrapper' => 'table--allowed-views',
            'event' => 'click',
          ],
          '#disabled' => ($i == 0 && $rows <= 1) ? TRUE : NULL,
          '#access' => ($i + 1 < $rows) ? FALSE : NULL,
        ],
      ];
    }
    $form[$element_wrapper_id][$element_id . '__add_row'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Row'),
      '#submit' => ['::addRowAjax'],
      '#ajax' => [
        'callback' => '::allowedViewsAjaxCallback',
        'wrapper' => 'table--allowed-views',
        'event' => 'click',
      ],
    ];

    $form['content_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configure contents'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['content_wrapper']['enable_contents_automatically'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automatically'),
      '#description' => $this->t('Automatically enables new content types in the configuration when they are created.'),
      '#default_value' => $config->get('enable_contents_automatically'),
    ];
    $form['content_wrapper']['content_types_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content types'),
      '#description' => $this->t('Set contents on which the title should be linked to the content edition.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['content_wrapper']['content_types_wrapper']['all_content_types'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('-- All content types --'),
      '#default_value' => $config->get('all_content_types'),
    ];
    $form['content_wrapper']['content_types_wrapper']['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getKeyedNodeTypes(),
      '#default_value' => array_keys(array_filter($config->get('content_types'))) ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('content_title_links_to_edition.settings')
      ->set('allowed_views', $this->getFormTableValues('allowed_views', ['enabled', 'view', 'title'], $form_state))
      ->set('enable_contents_automatically', $form_state->getValue('enable_contents_automatically'))
      ->set('all_content_types', $form_state->getValue('all_content_types'))
      ->set('content_types', $form_state->getValue('content_types'))
      ->save();

    $this->invalidateAllowedViewsCache();
  }

  /**
   * Allowed Views "add table row" AJAX callback.
   *
   * @param array &$form
   *   Drupal form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form's state object.
   */
  public function addRowAjax(array &$form, FormStateInterface $form_state): void {
    $form_state->set('allowed_views', $this->getFormTableRows('allowed_views', $form_state) + 1)->setRebuild();
  }

  /**
   * Allowed Views "remove table row" AJAX callback.
   *
   * @param array &$form
   *   Drupal form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form's state object.
   */
  public function removeRowAjax(array &$form, FormStateInterface $form_state): void {
    $rows = $this->getFormTableRows('allowed_views', $form_state);
    ($rows > 1) ? $form_state->set('allowed_views', $rows - 1) : NULL;
    $form_state->setRebuild();
  }

  /**
   * Allowed Views table AJAX callback.
   *
   * @param array &$form
   *   Drupal form's render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form's state object.
   *
   * @return array
   *   Allowed Views Drupal's render array.
   */
  public function allowedViewsAjaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form['allowed_views__wrapper']['allowed_views'] ?? [];
  }

  /**
   * Get given form element table row count.
   *
   * @param string $element_id
   *   Form element ID.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form's state object.
   *
   * @return int
   *   Given form table's row count.
   */
  private function getFormTableRows(string $element_id, FormStateInterface $form_state): int {
    return (int) ($form_state->get($element_id) ?? (count($this->config('content_title_links_to_edition.settings')->get($element_id)) ?? 1));
  }

  /**
   * Get given form table values array.
   *
   * @param string $element_id
   *   Form element ID.
   * @param array $element_data_structure
   *   Form element data structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form's state object.
   *
   * @return array
   *   Given form table values array.
   */
  private function getFormTableValues(string $element_id, array $element_data_structure, FormStateInterface $form_state): array {
    $table_values = [];

    $rows = $this->getFormTableRows('allowed_views', $form_state);
    for ($i = 0; $i < $rows; $i++) {
      $data_values = [];
      foreach ($element_data_structure as $element_data) {
        $data_values[$element_data] = $form_state->getValue([$element_id, $i, $element_data]);
      }

      (!empty($data_values)) ? $table_values[] = $data_values : NULL;
    }

    return $table_values;
  }

  /**
   * Get site contents' ID and label keyed in an array.
   *
   * @return array
   *   Keyed site's content types.
   */
  private function getKeyedNodeTypes(): array {
    /** @var \Drupal\Core\Entity\EntityInterface[]|null $content_types */
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $keyed_content_types = [];
    foreach ($content_types as $content_type) {
      if ($content_type instanceof NodeType && $content_type->status()) {
        $keyed_content_types[$content_type->id()] = $content_type->label();
      }
    }

    return $keyed_content_types;
  }

  /**
   * Invalidate configured "Allowed views" cache tags.
   */
  private function invalidateAllowedViewsCache(): void {
    $views = $this->config('content_title_links_to_edition.settings')->get('allowed_views');
    foreach ($views as $view) {
      if (isset($view['enabled']) && isset($view['view']) && $view['enabled'] && strlen($view['view']) > 0) {
        $view = $this->entityTypeManager->getStorage('view')->load($view['view']);
        if ($view instanceof View) {
          Cache::invalidateTags($view->getCacheTags());
        }
      }
    }
  }

}
