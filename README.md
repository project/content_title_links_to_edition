[![Pablo Urrea Badge](https://pablourrea.github.io/sites/default/files/badges/pablourrea.svg)](https://pablourrea.github.io)
![by Biko2](https://raw.githubusercontent.com/biko2/biko-repo-bagdes/master/png/biko-bagge-pill.png)


# Content Title Links to Edition

## Introduction

The [Content Title Links to Edition](https://www.drupal.org/project/content_title_links_to_edition/) module provides a quick out of the box solution for linking the Content view node title to the entity edit form. This makes Drupal easier to use for editors who do not need to access the content view from the Contents view or are unfamiliar with the CMS, a usage flow that is very convenient in headless Drupal installations, for example.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Content Title Links to Edition module at Administration > Extend.
2. Configure the module at Administration > Configuration > Content Authoring > Content Title Links to Edition Settings.


## Current Active Maintainer

- Pablo Urrea - [enchufe](https://www.drupal.org/u/enchufe)


## Bug Reports & New Features

Please report bugs to the [Drupal.org issue queue](https://www.drupal.org/project/issues/content_title_links_to_edition).

Note that the maintainers are not actively supporting development of new
features. However, contributions to add features are welcome provided they are
narrow in scope, well written, and well documented.
